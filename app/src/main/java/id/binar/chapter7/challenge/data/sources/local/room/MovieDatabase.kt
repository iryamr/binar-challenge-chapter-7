package id.binar.chapter7.challenge.data.sources.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import id.binar.chapter7.challenge.data.sources.local.entities.MovieEntity
import id.binar.chapter7.challenge.data.sources.local.entities.ProfileEntity
import id.binar.chapter7.challenge.data.sources.local.entities.UserEntity
import id.binar.chapter7.challenge.data.sources.local.room.dao.MovieDao
import id.binar.chapter7.challenge.data.sources.local.room.dao.UserDao

@Database(
    entities = [
        UserEntity::class,
        ProfileEntity::class,
        MovieEntity::class,
    ],
    version = 1,
    exportSchema = false
)
abstract class MovieDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun movieDao(): MovieDao
}