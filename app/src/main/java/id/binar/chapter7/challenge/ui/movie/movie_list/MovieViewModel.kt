package id.binar.chapter7.challenge.ui.movie.movie_list

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter7.challenge.domain.repositories.MovieRepository
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(
    private val repository: MovieRepository
) : ViewModel() {

    fun getNowPlayingMovies() = repository.getNowPlayingMovies()

    fun getPopularMovies() = repository.getPopularMovies()
}