package id.binar.chapter7.challenge.domain.models.movie

import android.os.Parcelable
import id.binar.chapter7.challenge.utils.CommonType
import kotlinx.parcelize.Parcelize

@Parcelize
data class Movie(
    val id: Int,
    var userId: Int = 0,
    val movieId: Int = 0,
    val title: String,
    val posterPath: String,
    val backdropPath: String,
    val releaseDate: String,
    val genres: List<Genre>? = null,
    val overview: String,
    val popularity: Double = 0.0,
    val voteAverage: Double = 0.0,
) : Parcelable, CommonType