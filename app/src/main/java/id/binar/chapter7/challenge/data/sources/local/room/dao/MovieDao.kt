package id.binar.chapter7.challenge.data.sources.local.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import id.binar.chapter7.challenge.data.sources.local.entities.MovieEntity

@Dao
interface MovieDao {

    @Query("SELECT * FROM movies WHERE user_id = :userId")
    suspend fun getFavoriteMovies(userId: Int): List<MovieEntity>

    @Query("SELECT * FROM movies WHERE user_id = :userId AND movie_id = :movieId")
    suspend fun getFavoriteMovie(userId: Int, movieId: Int): MovieEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovies(movies: List<MovieEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFavoriteMovie(movie: MovieEntity)

    @Query("DELETE FROM movies")
    suspend fun deleteMovies()

    @Query("DELETE FROM movies WHERE user_id = :userId AND movie_id = :movieId")
    suspend fun deleteFavoriteMovie(userId: Int, movieId: Int?)
}