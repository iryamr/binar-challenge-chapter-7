package id.binar.chapter7.challenge.ui.movie.movie_favorite

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter7.challenge.domain.repositories.MovieRepository
import javax.inject.Inject

@HiltViewModel
class FavoriteViewModel @Inject constructor(
    private val repository: MovieRepository
) : ViewModel() {

    fun getFavoriteMovies(userId: Int) = repository.getFavoriteMovies(userId)
}