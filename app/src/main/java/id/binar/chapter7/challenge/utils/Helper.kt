package id.binar.chapter7.challenge.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Base64
import android.widget.Toast
import id.binar.chapter7.challenge.R
import java.io.ByteArrayOutputStream

object Helper {

    fun setTitleWithYear(context: Context, title: String, year: String?): SpannableStringBuilder {
        val text = context.getString(R.string.title, title, year)

        val titleStart = text.indexOf(title)
        val titleEnd = titleStart + title.length

        val yearColor = Color.GRAY
        val yearStart = text.indexOf(year!!)
        val yearEnd = yearStart + year.length

        return SpannableStringBuilder(text).apply {
            setSpan(
                StyleSpan(Typeface.BOLD),
                titleStart,
                titleEnd,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            setSpan(
                ForegroundColorSpan(yearColor),
                yearStart,
                yearEnd,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }

    fun showToast(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun bitmapToString(bitmap: Bitmap): String {
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
        val ba = baos.toByteArray()
        return Base64.encodeToString(ba, Base64.DEFAULT)
    }

    fun stringToBitmap(encodedString: String?): Bitmap? {
        return try {
            val decodeByte = Base64.decode(encodedString, Base64.DEFAULT)
            BitmapFactory.decodeByteArray(decodeByte, 0, decodeByte.size)
        } catch (e: Exception) {
            e.message
            null
        }
    }
}