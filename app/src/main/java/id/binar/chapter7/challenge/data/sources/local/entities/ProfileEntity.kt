package id.binar.chapter7.challenge.data.sources.local.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "profile")
data class ProfileEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    var userId: Int = 0,
    var name: String? = null,
    var birthday: String? = null,
    var address: String? = null,
    var photo: String? = null,
)
