package id.binar.chapter7.challenge.data.sources.remote.service

import id.binar.chapter7.challenge.data.sources.remote.responses.*
import id.binar.chapter7.challenge.utils.Endpoints
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET(Endpoints.NOW_PLAYING_MOVIES)
    suspend fun fetchNowPlayingMovies(): BaseDto<NowPlayingMovieDto>

    @GET(Endpoints.POPULAR_MOVIES)
    suspend fun fetchPopularMovies(): BaseDto<PopularMovieDto>

    @GET(Endpoints.MOVIE_DETAIL)
    suspend fun fetchMovieDetails(
        @Path("movie_id") movieId: Int
    ): MovieDetailDto

    @GET(Endpoints.MOVIE_CASTS)
    suspend fun fetchMovieCast(
        @Path("movie_id") movieId: Int,
    ): CastDto
}