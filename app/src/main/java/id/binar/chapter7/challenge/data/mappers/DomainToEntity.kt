package id.binar.chapter7.challenge.data.mappers

import id.binar.chapter7.challenge.data.sources.local.entities.MovieEntity
import id.binar.chapter7.challenge.data.sources.local.entities.ProfileEntity
import id.binar.chapter7.challenge.data.sources.local.entities.UserEntity
import id.binar.chapter7.challenge.domain.models.auth.Profile
import id.binar.chapter7.challenge.domain.models.auth.User
import id.binar.chapter7.challenge.domain.models.movie.MovieDetail

fun User.toEntity(): UserEntity =
    UserEntity(
        id = id,
        username = username,
        email = email,
        password = password,
    )

fun Profile.toEntity(): ProfileEntity =
    ProfileEntity(
        id = id,
        userId = userId,
        name = name,
        address = address,
        birthday = birthday,
        photo = photo,
    )

fun MovieDetail.toEntity(userId: Int): MovieEntity =
    MovieEntity(
        movieId = id,
        userId = userId,
        title = title,
        posterPath = posterPath,
        overview = overview,
        voteAverage = voteAverage,
    )