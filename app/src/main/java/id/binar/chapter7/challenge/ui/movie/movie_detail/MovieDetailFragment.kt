package id.binar.chapter7.challenge.ui.movie.movie_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import id.binar.chapter7.challenge.databinding.FragmentMovieDetailBinding
import id.binar.chapter7.challenge.domain.models.Result
import id.binar.chapter7.challenge.domain.models.movie.CastDetail
import id.binar.chapter7.challenge.domain.models.movie.Genre
import id.binar.chapter7.challenge.domain.models.movie.MovieDetail
import id.binar.chapter7.challenge.ui.adapter.CastAdapter
import id.binar.chapter7.challenge.utils.Extensions.loadImage
import id.binar.chapter7.challenge.utils.Extensions.toDate
import id.binar.chapter7.challenge.utils.Extensions.toRating
import id.binar.chapter7.challenge.utils.Helper.setTitleWithYear

@AndroidEntryPoint
class MovieDetailFragment : Fragment() {

    private var _binding: FragmentMovieDetailBinding? = null
    private val binding get() = _binding!!

    private val args: MovieDetailFragmentArgs by navArgs()
    private val viewModel: MovieDetailViewModel by viewModels()

    private val castAdapter: CastAdapter by lazy { CastAdapter() }

    private val movieId: Int by lazy { args.movieId }
    private val userId: Int by lazy { args.userId }

    private var movie: MovieDetail? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMovieDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ViewCompat.getWindowInsetsController(requireActivity().window.decorView)?.isAppearanceLightStatusBars =
            false

        initUI()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initUI() {
        viewModel.getMovieDetail(movieId).observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> {
                    setLoadState(true)
                }
                is Result.Success -> {
                    setLoadState(false)
                    movie = result.data
                    initMovieDetails(movie!!)
                }
                is Result.Error -> {
                    setLoadState(false)
                    setErrorState(true)
                }
            }
        }

        viewModel.getMovieCasts(movieId).observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> {
                    setLoadState(true)
                }
                is Result.Success -> {
                    setLoadState(false)
                    initCasts(result.data)
                }
                is Result.Error -> {
                    setLoadState(false)
                    setErrorState(true)
                }
            }
        }

        viewModel.getFavoriteMovie(userId, movieId).observe(viewLifecycleOwner) { result ->
            val isFavorite = result?.movieId == movieId
            binding.content.toggleFavorite.setOnClickListener {
                if (result == null) {
                    viewModel.addToFavorite(movie!!, userId)
                } else {
                    viewModel.removeFromFavorite(userId, result.movieId)
                }
            }
            binding.content.toggleFavorite.isChecked = isFavorite
        }

        binding.content.btnBack.setOnClickListener { findNavController().popBackStack() }
    }

    private fun initMovieDetails(movie: MovieDetail) {
        binding.content.apply {
            tvTitle.text =
                setTitleWithYear(requireContext(), movie.title, movie.releaseDate.toDate())
            tvOverview.text = movie.overview
            tvGenre.text = initGenres(movie.genres)
            ivBackdrop.loadImage(movie.backdropPath)
            ivPoster.loadImage(movie.posterPath)
            ratingBar.rating = movie.voteAverage.toRating()
        }
    }

    private fun initGenres(genres: List<Genre>?): String {
        val list = ArrayList<Genre>()
        genres?.forEach { genre -> list.add(genre) }
        return list.joinToString { it.name }
    }

    private fun initCasts(casts: List<CastDetail>) {
        castAdapter.submitList(casts)
        binding.content.rvCasts.adapter = castAdapter
        binding.content.rvCasts.layoutManager = initLayoutManager()
    }

    private fun initLayoutManager(): LinearLayoutManager {
        return LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
    }

    private fun setLoadState(isLoading: Boolean) {
        binding.apply {
            progressBar.isVisible = isLoading
            content.root.isVisible = !isLoading
        }
    }

    private fun setErrorState(isLoading: Boolean) {
        binding.apply {
            error.root.isVisible = isLoading
        }
    }
}