package id.binar.chapter7.challenge.data.repositories.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import id.binar.chapter7.challenge.data.mappers.toDomain
import id.binar.chapter7.challenge.data.mappers.toEntity
import id.binar.chapter7.challenge.data.sources.local.room.dao.MovieDao
import id.binar.chapter7.challenge.data.sources.remote.service.ApiService
import id.binar.chapter7.challenge.domain.models.Result
import id.binar.chapter7.challenge.domain.models.movie.*
import id.binar.chapter7.challenge.domain.repositories.MovieRepository
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    private val movieDao: MovieDao
) : MovieRepository {

    override fun getNowPlayingMovies(): LiveData<Result<List<NowPlayingMovie>>> = liveData {
        emit(Result.Loading)
        try {
            val response = apiService.fetchNowPlayingMovies()
            val data = response.results.map { it.toDomain() }
            emit(Result.Success(data))
        } catch (e: HttpException) {
            emit(Result.Error("Please check your connection"))
        } catch (e: IOException) {
            emit(Result.Error("Something is wrong"))
        }
    }

    override fun getPopularMovies(): LiveData<Result<List<PopularMovie>>> = liveData {
        emit(Result.Loading)
        try {
            val response = apiService.fetchPopularMovies()
            val data = response.results.map { it.toDomain() }
            emit(Result.Success(data))
        } catch (e: HttpException) {
            emit(Result.Error("Please check your connection"))
        } catch (e: IOException) {
            emit(Result.Error("Something is wrong"))
        } catch (e: NullPointerException) {
            emit(Result.Error("Data not found"))
        }
    }

    override fun getFavoriteMovies(userId: Int): LiveData<Result<List<FavoriteMovie>>> = liveData {
        emit(Result.Loading)
        try {
            val data = movieDao.getFavoriteMovies(userId).map { it.toDomain() }
            emit(Result.Success(data))
        } catch (e: NullPointerException) {
            emit(Result.Error("Favorite movies not found"))
        } catch (e: IOException) {
            emit(Result.Error("Something is wrong"))
        }
    }

    override fun getFavoriteMovie(userId: Int, movieId: Int): LiveData<FavoriteMovie?> = liveData {
        emit(movieDao.getFavoriteMovie(userId, movieId)?.toDomain())
    }

    override fun getMovieDetail(movieId: Int): LiveData<Result<MovieDetail>> = liveData {
        emit(Result.Loading)
        try {
            val response = apiService.fetchMovieDetails(movieId).toDomain()
            emit(Result.Success(response))
        } catch (e: NullPointerException) {
            emit(Result.Error("Movie not found"))
        } catch (e: IOException) {
            emit(Result.Error("Something is wrong"))
        } catch (e: HttpException) {
            emit(Result.Error("Please check your connection"))
        }
    }

    override fun getMovieCasts(movieId: Int): LiveData<Result<List<CastDetail>>> = liveData {
        emit(Result.Loading)
        try {
            val response = apiService.fetchMovieCast(movieId)
            val data = response.cast.map { it.toDomain() }
            emit(Result.Success(data))
        } catch (e: NullPointerException) {
            emit(Result.Error("Data not found"))
        } catch (e: IOException) {
            emit(Result.Error("Something is wrong"))
        } catch (e: HttpException) {
            emit(Result.Error("Please check your connection"))
        }
    }

    override suspend fun addToFavorite(movie: MovieDetail, userId: Int) {
        movieDao.insertFavoriteMovie(movie.toEntity(userId))
    }

    override suspend fun removeFromFavorite(userId: Int, movieId: Int?) {
        movieDao.deleteFavoriteMovie(userId, movieId)
    }
}