package id.binar.chapter7.challenge.ui.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

open class BaseVerticalAdapter<T> :
    ListAdapter<T, BaseVerticalAdapter.BaseVerticalViewHolder<T>>(BaseItemCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseVerticalViewHolder<T> {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return BaseVerticalViewHolder(view)
    }

    override fun onBindViewHolder(holder: BaseVerticalViewHolder<T>, position: Int) {
        holder.onBind(currentList[position])
    }

    open class BaseVerticalViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {

        open fun onBind(data: T) {}
    }
}
