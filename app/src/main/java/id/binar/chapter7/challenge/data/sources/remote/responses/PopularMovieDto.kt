package id.binar.chapter7.challenge.data.sources.remote.responses

import com.google.gson.annotations.SerializedName

data class PopularMovieDto(

    @SerializedName("id")
    val id: Int,

    @SerializedName("title")
    val title: String,

    @SerializedName("poster_path")
    val posterPath: String,

    @SerializedName("overview")
    val overview: String,

    @SerializedName("vote_average")
    val voteAverage: Double,
)