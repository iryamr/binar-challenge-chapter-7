package id.binar.chapter7.challenge.data.sources.local.entities

import androidx.room.Embedded
import androidx.room.Relation

data class UserAndProfile(
    @Embedded
    val user: UserEntity,

    @Relation(
        parentColumn = "id",
        entityColumn = "userId"
    )
    var profile: ProfileEntity? = null
)
