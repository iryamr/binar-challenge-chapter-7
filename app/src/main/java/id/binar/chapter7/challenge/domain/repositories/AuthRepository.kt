package id.binar.chapter7.challenge.domain.repositories

import androidx.lifecycle.LiveData
import id.binar.chapter7.challenge.data.sources.local.entities.UserAndProfile
import id.binar.chapter7.challenge.domain.models.Result
import id.binar.chapter7.challenge.domain.models.auth.Profile
import id.binar.chapter7.challenge.domain.models.auth.User

interface AuthRepository {

    fun signUp(username: String, email: String, password: String): LiveData<Result<User>>

    fun signIn(email: String, password: String): LiveData<Result<User>>

    fun getUser(email: String): LiveData<Result<User>>

    suspend fun updateUser(user: User): Result<Int>

    fun getProfile(userId: Int): LiveData<Result<UserAndProfile>>

    suspend fun createProfile(profile: Profile): Result<Long>

    suspend fun updateProfile(profile: Profile): Result<Int>

    fun getAuthenticatedUser(): LiveData<String>

    fun logout()
}