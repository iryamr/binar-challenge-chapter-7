package id.binar.chapter7.challenge.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import id.binar.chapter7.challenge.databinding.ItemMoviesHorizontalBinding
import id.binar.chapter7.challenge.domain.models.movie.NowPlayingMovie
import id.binar.chapter7.challenge.utils.Extensions.loadImage
import id.binar.chapter7.challenge.utils.Extensions.toRating

class MovieHorizontalAdapter(
    private val onClick: (NowPlayingMovie) -> Unit
) : ListAdapter<NowPlayingMovie, MovieHorizontalAdapter.MovieViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val binding =
            ItemMoviesHorizontalBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class MovieViewHolder(private val binding: ItemMoviesHorizontalBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(movie: NowPlayingMovie) {
            with(binding) {
                tvTitle.text = movie.title
                ratingBar.rating = movie.voteAverage.toRating()
                ivPoster.loadImage(movie.posterPath)
                cvPoster.setOnClickListener { onClick(movie) }
                root.setOnClickListener { onClick(movie) }
            }
        }
    }

    companion object {

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<NowPlayingMovie>() {
            override fun areItemsTheSame(
                oldItem: NowPlayingMovie,
                newItem: NowPlayingMovie
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: NowPlayingMovie,
                newItem: NowPlayingMovie
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}