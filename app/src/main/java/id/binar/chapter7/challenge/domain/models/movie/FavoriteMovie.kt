package id.binar.chapter7.challenge.domain.models.movie

import id.binar.chapter7.challenge.utils.CommonType

data class FavoriteMovie(
    val id: Int,
    val userId: Int = 0,
    val movieId: Int = 0,
    val title: String,
    val posterPath: String,
    val overview: String,
    val voteAverage: Double,
) : CommonType
