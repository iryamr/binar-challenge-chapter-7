package id.binar.chapter7.challenge.ui.profile

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import id.binar.chapter7.challenge.data.mappers.toDomain
import id.binar.chapter7.challenge.data.sources.local.entities.UserAndProfile
import id.binar.chapter7.challenge.databinding.FragmentProfileBinding
import id.binar.chapter7.challenge.domain.models.Result
import id.binar.chapter7.challenge.domain.models.auth.Profile
import id.binar.chapter7.challenge.domain.models.auth.User
import id.binar.chapter7.challenge.ui.MainViewModel
import id.binar.chapter7.challenge.utils.Extensions.loadImage
import id.binar.chapter7.challenge.utils.Helper.bitmapToString
import id.binar.chapter7.challenge.utils.Helper.showToast
import id.binar.chapter7.challenge.utils.Helper.stringToBitmap

@AndroidEntryPoint
class ProfileFragment : Fragment() {

    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    private var user: User? = null
    private var profile: Profile? = null

    private val viewModel: MainViewModel by activityViewModels()

    private val args: ProfileFragmentArgs by navArgs()

    private val galleryResult =
        registerForActivityResult(ActivityResultContracts.GetContent()) { result ->
            var uriString = result.toString()
            uriString = "1_$uriString"
            profile?.photo = uriString
            binding.ivProfile.loadImage(result)
        }

    private val cameraResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK && result.data != null) {
                val bitmap = result.data?.extras?.get("data") as Bitmap
                var bitmapString = bitmapToString(bitmap)
                bitmapString = "2_$bitmapString"
                profile?.photo = bitmapString
                binding.ivProfile.loadImage(bitmap)
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        chooseImage()
        doObserveProfile()
        doObserveUpdateProfile()
        logout()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun doObserveProfile() {
        viewModel.getProfile(args.userId).observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> {}
                is Result.Success -> {
                    setDataProfile(result.data)
                }
                is Result.Error -> {
                    showToast(requireContext(), result.error)
                }
            }
        }
    }

    private fun doObserveUpdateProfile() {
        binding.btnUpdate.setOnClickListener {
            val username = binding.etUsername.text.toString()
            val name = binding.etName.text.toString()
            val birthday = binding.etDateOfBirth.text.toString()
            val address = binding.etAddress.text.toString()

            if (validateData(username, name, birthday, address)) {
                user?.let { dataUser ->
                    dataUser.username = username
                    profile?.let { dataProfile ->
                        dataProfile.userId = args.userId
                        dataProfile.name = name
                        dataProfile.birthday = birthday
                        dataProfile.address = address
                        dataProfile.photo = profile?.photo

                        updateProfile(dataUser, dataProfile)
                    }
                }
            }
        }
    }

    private fun setDataProfile(data: UserAndProfile) {
        user = data.user.toDomain()
        profile = data.profile?.toDomain()

        binding.etUsername.setText(data.user.username)
        binding.etName.setText(data.profile?.name)
        binding.etDateOfBirth.setText(data.profile?.birthday)
        binding.etAddress.setText(data.profile?.address)
        if (data.profile?.photo != null) {
            setPhotoProfile(data.profile?.photo!!)
        }
    }

    private fun setPhotoProfile(value: String) {
        when (value.substring(0, 1)) {
            "1" -> {
                val data = value.substringAfter("_")
                val uri = Uri.parse(data)
                binding.ivProfile.loadImage(uri)
            }
            "2" -> {
                val data = value.substringAfter("_")
                val bitmap = stringToBitmap(data)
                binding.ivProfile.loadImage(bitmap)
            }
        }
    }

    private fun updateProfile(user: User, profile: Profile) {
        viewModel.updateProfile(user, profile).observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> {}
                is Result.Success -> {
                    showToast(requireContext(), "Profile updated")
                    findNavController().popBackStack()
                }
                is Result.Error -> {
                    showToast(requireContext(), result.error)
                }
            }
        }
    }

    private fun validateData(
        username: String,
        name: String,
        birthday: String,
        address: String
    ): Boolean {
        return when {
            username.isEmpty() -> {
                binding.etlUsername.error = "Username tidak boleh kosong"
                binding.etlUsername.requestFocus()
                false
            }
            name.isEmpty() -> {
                binding.etlName.error = "Nama tidak boleh kosong"
                binding.etlName.requestFocus()
                false
            }
            birthday.isEmpty() -> {
                binding.etlDateOfBirth.error = "Tanggal lahir tidak boleh kosong"
                binding.etlDateOfBirth.requestFocus()
                false
            }
            address.isEmpty() -> {
                binding.etlAddress.error = "Alamat"
                binding.etlAddress.requestFocus()
                false
            }
            else -> true
        }
    }

    private fun chooseImage() {
        binding.ivProfile.setOnClickListener {
            checkPermissions()
        }
    }

    private fun checkPermissions() {
        if (checkIfPermissionsIsGranted(
                requireActivity(),
                Manifest.permission.CAMERA,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                REQUEST_CODE_PERMISSION
            )
        ) {
            chooseImageDialog()
        }
    }

    private fun checkIfPermissionsIsGranted(
        activity: Activity,
        permission: String,
        permissions: Array<String>,
        requestCode: Int
    ): Boolean {
        val permissionCheck = ActivityCompat.checkSelfPermission(activity, permission)
        return if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                showPermissionDeniedDialog()
            } else {
                ActivityCompat.requestPermissions(activity, permissions, requestCode)
            }
            false
        } else {
            true
        }
    }

    private fun showPermissionDeniedDialog() {
        AlertDialog.Builder(requireContext())
            .setTitle("Permission Denied")
            .setMessage("Permission is denied, please allow permissions from App Settings.")
            .setPositiveButton("App Settings") { _, _ -> openAppSettings() }
            .setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }
            .show()
    }

    private fun chooseImageDialog() {
        AlertDialog.Builder(requireContext())
            .setMessage("Choose an Image")
            .setPositiveButton("Gallery") { _, _ -> openGallery() }
            .setNegativeButton("Camera") { _, _ -> openCamera() }
            .show()
    }

    private fun openAppSettings() {
        val intent = Intent()
        val uri = Uri.fromParts("package", requireActivity().packageName, null)

        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        intent.data = uri

        startActivity(intent)
    }

    private fun openGallery() {
        requireActivity().intent.type = "image/*"
        galleryResult.launch("image/*")
    }

    private fun openCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { cameraResult.launch(it) }
    }

    private fun logout() {
        binding.btnLogout.setOnClickListener {
            viewModel.logout()
            val directions = ProfileFragmentDirections.actionProfileFragmentToSignInFragment()
            findNavController().navigate(directions)
        }
    }

    companion object {
        private const val REQUEST_CODE_PERMISSION = 100
    }
}
