package id.binar.chapter7.challenge.domain.models.movie

data class MovieDetail(
    val id: Int,
    var userId: Int = 0,
    val title: String,
    val genres: List<Genre>,
    val posterPath: String,
    val backdropPath: String,
    val releaseDate: String,
    val overview: String,
    val voteAverage: Double,
)