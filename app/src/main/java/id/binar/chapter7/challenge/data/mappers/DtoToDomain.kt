package id.binar.chapter7.challenge.data.mappers

import id.binar.chapter7.challenge.data.sources.remote.responses.*
import id.binar.chapter7.challenge.domain.models.movie.*

fun NowPlayingMovieDto.toDomain(): NowPlayingMovie {
    return NowPlayingMovie(
        id = id,
        title = title,
        posterPath = posterPath,
        overview = overview,
        voteAverage = voteAverage,
    )
}

fun PopularMovieDto.toDomain(): PopularMovie {
    return PopularMovie(
        id = id,
        title = title,
        posterPath = posterPath,
        overview = overview,
        voteAverage = voteAverage,
    )
}

fun MovieDetailDto.toDomain(): MovieDetail {
    val genres = genres.map { it.toDomain() }

    return MovieDetail(
        id = id,
        title = title,
        genres = genres,
        posterPath = posterPath,
        backdropPath = backdropPath,
        releaseDate = releaseDate,
        overview = overview,
        voteAverage = voteAverage,
    )
}

fun GenreDto.toDomain(): Genre =
    Genre(
        id = id,
        name = name
    )

fun CastDetailDto.toDomain(): CastDetail =
    CastDetail(
        id = id,
        movieId = movieId,
        castId = castId,
        name = name,
        originalName = originalName,
        character = character,
        profilePath = profilePath
    )