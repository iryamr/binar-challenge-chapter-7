package id.binar.chapter7.challenge.domain.models.auth

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(
    var id: Int = 0,
    var username: String,
    var email: String,
    var password: String
) : Parcelable
