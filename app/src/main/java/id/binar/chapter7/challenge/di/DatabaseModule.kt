package id.binar.chapter7.challenge.di

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import id.binar.chapter7.challenge.data.sources.local.room.MovieDatabase
import id.binar.chapter7.challenge.data.sources.local.room.dao.MovieDao
import id.binar.chapter7.challenge.data.sources.local.room.dao.UserDao
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(app: Application): MovieDatabase =
        Room.databaseBuilder(app, MovieDatabase::class.java, "Movie.db").build()

    @Provides
    fun provideMovieDao(database: MovieDatabase): MovieDao =
        database.movieDao()

    @Provides
    fun provideUserDao(database: MovieDatabase): UserDao =
        database.userDao()
}