package id.binar.chapter7.challenge.ui.movie.movie_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.core.view.isInvisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import id.binar.chapter7.challenge.R
import id.binar.chapter7.challenge.databinding.FragmentMovieBinding
import id.binar.chapter7.challenge.domain.models.Result
import id.binar.chapter7.challenge.domain.models.movie.NowPlayingMovie
import id.binar.chapter7.challenge.domain.models.movie.PopularMovie
import id.binar.chapter7.challenge.ui.MainViewModel
import id.binar.chapter7.challenge.ui.adapter.MovieHorizontalAdapter
import id.binar.chapter7.challenge.ui.adapter.MovieVerticalAdapter
import id.binar.chapter7.challenge.utils.Helper.showToast

@AndroidEntryPoint
class MovieFragment : Fragment() {

    private var _binding: FragmentMovieBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MovieViewModel by viewModels()
    private val sharedViewModel: MainViewModel by activityViewModels()

    private val args: MovieFragmentArgs by navArgs()
    private val email: String by lazy { args.email }

    private var userId: Int = 0

    private val nowPlayingMovieAdapter: MovieHorizontalAdapter by lazy {
        MovieHorizontalAdapter {
            val directions =
                MovieFragmentDirections.actionMovieFragmentToMovieDetailFragment(it.id, userId)
            findNavController().navigate(directions)
        }
    }

    private val popularMovieAdapter: MovieVerticalAdapter<PopularMovie> by lazy {
        MovieVerticalAdapter {
            val directions =
                MovieFragmentDirections.actionMovieFragmentToMovieDetailFragment(it.id, userId)
            findNavController().navigate(directions)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMovieBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ViewCompat.getWindowInsetsController(requireActivity().window.decorView)?.isAppearanceLightStatusBars =
            true

        initView()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initView() {
        doObserveUser()
        doObserveNowPlayingMovies()
        doObservePopularMovies()
    }

    private fun doObserveUser() {
        sharedViewModel.getUser(email).observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> {
                    showShimmerUser()
                }
                is Result.Success -> {
                    hideShimmerUser()
                    userId = result.data.id
                    binding.content.tvGreetingHome.text =
                        getString(R.string.greeting, result.data.username)
                    onProfileClicked(userId)
                    onFavoriteClicked(userId)
                }
                is Result.Error -> {
                    showToast(requireContext(), result.error)
                    hideShimmerUser()
                }
            }
        }
    }

    private fun doObserveNowPlayingMovies() {
        viewModel.getNowPlayingMovies().observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> {
                    showShimmerHorizontal()
                }
                is Result.Success -> {
                    hideShimmerHorizontal()
                    initNowPlayingMovies(result.data)
                }
                is Result.Error -> {
                    showToast(requireContext(), result.error)
                    hideShimmerHorizontal()
                }
            }
        }
    }

    private fun doObservePopularMovies() {
        viewModel.getPopularMovies().observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> {
                    showShimmerVertical()
                }
                is Result.Success -> {
                    hideShimmerVertical()
                    initPopularMovies(result.data)
                }
                is Result.Error -> {
                    hideShimmerVertical()
                }
            }
        }
    }

    private fun initNowPlayingMovies(movies: List<NowPlayingMovie>) {
        nowPlayingMovieAdapter.submitList(movies)
        binding.content.rvNowPlayingMovies.adapter = nowPlayingMovieAdapter
        binding.content.rvNowPlayingMovies.layoutManager = initLayoutManager(HORIZONTAL)
    }

    private fun initPopularMovies(movies: List<PopularMovie>) {
        popularMovieAdapter.submitList(movies)
        binding.content.rvPopularMovies.adapter = popularMovieAdapter
        binding.content.rvPopularMovies.layoutManager = initLayoutManager(VERTICAL)
    }

    private fun initLayoutManager(oritentation: Int): LinearLayoutManager {
        return LinearLayoutManager(requireContext(), oritentation, false)
    }

    private fun onProfileClicked(userId: Int) {
        binding.content.ivProfile.setOnClickListener {
            showToast(requireContext(), "tes")
            val directions = MovieFragmentDirections.actionMovieFragmentToProfileFragment(userId)
            findNavController().navigate(directions)
        }
    }

    private fun onFavoriteClicked(userId: Int) {
        binding.content.ivFavorite.setOnClickListener {
            val directions = MovieFragmentDirections.actionMovieFragmentToFavoriteFragment(userId)
            findNavController().navigate(directions)
        }
    }

    private fun showShimmerUser() {
        binding.apply {
            shimmer.shimmerUser.startShimmer()
            content.tvGreetingHome.isInvisible = true
        }
    }

    private fun hideShimmerUser() {
        binding.apply {
            shimmer.shimmerUser.isInvisible = true
            shimmer.shimmerUser.stopShimmer()
            content.tvGreetingHome.isInvisible = false
        }
    }

    private fun showShimmerHorizontal() {
        binding.apply {
            shimmer.shimmerHorizontal.startShimmer()
            content.rvNowPlayingMovies.isInvisible = true
        }
    }

    private fun hideShimmerHorizontal() {
        binding.apply {
            shimmer.shimmerHorizontal.isInvisible = true
            shimmer.shimmerHorizontal.stopShimmer()
            content.rvNowPlayingMovies.isInvisible = false
        }
    }

    private fun showShimmerVertical() {
        binding.apply {
            shimmer.shimmerHorizontal.startShimmer()
            content.rvPopularMovies.isInvisible = true
        }
    }

    private fun hideShimmerVertical() {
        binding.apply {
            shimmer.shimmerVertical.isInvisible = true
            shimmer.shimmerVertical.stopShimmer()
            content.rvPopularMovies.isInvisible = false
        }
    }

    companion object {
        private const val HORIZONTAL = LinearLayoutManager.HORIZONTAL
        private const val VERTICAL = LinearLayoutManager.VERTICAL
    }
}