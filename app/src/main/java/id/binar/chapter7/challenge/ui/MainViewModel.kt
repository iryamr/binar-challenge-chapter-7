package id.binar.chapter7.challenge.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter7.challenge.domain.models.Result
import id.binar.chapter7.challenge.domain.models.auth.Profile
import id.binar.chapter7.challenge.domain.models.auth.User
import id.binar.chapter7.challenge.domain.repositories.AuthRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: AuthRepository
) : ViewModel() {

    private val update = MutableLiveData<Result<Int>>()

    val auth = repository.getAuthenticatedUser()

    fun signUp(username: String, email: String, password: String) =
        repository.signUp(username, email, password)

    fun signIn(email: String, password: String) =
        repository.signIn(email, password)

    fun getUser(email: String) =
        repository.getUser(email)

    fun getProfile(userId: Int) = repository.getProfile(userId)

    fun updateProfile(
        user: User,
        profile: Profile
    ): LiveData<Result<Int>> {
        viewModelScope.launch {
            repository.updateUser(user)
            update.value = repository.updateProfile(profile)
        }
        return update
    }

    fun logout() = repository.logout()
}