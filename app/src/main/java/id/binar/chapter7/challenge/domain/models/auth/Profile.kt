package id.binar.chapter7.challenge.domain.models.auth

data class Profile(
    var id: Int = 0,
    var userId: Int = 0,
    var name: String? = null,
    var address: String? = null,
    var birthday: String? = null,
    var photo: String? = null,
)
