package id.binar.chapter7.challenge.data.repositories.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.liveData
import id.binar.chapter7.challenge.data.mappers.toDomain
import id.binar.chapter7.challenge.data.mappers.toEntity
import id.binar.chapter7.challenge.data.sources.local.entities.UserAndProfile
import id.binar.chapter7.challenge.data.sources.local.entities.UserEntity
import id.binar.chapter7.challenge.data.sources.local.room.dao.UserDao
import id.binar.chapter7.challenge.domain.models.Result
import id.binar.chapter7.challenge.domain.models.auth.Profile
import id.binar.chapter7.challenge.domain.models.auth.User
import id.binar.chapter7.challenge.domain.repositories.AuthRepository
import id.binar.chapter7.challenge.utils.UserPreferences
import java.io.IOException
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val userDao: UserDao,
    private val prefs: UserPreferences
) : AuthRepository {

    override fun signUp(
        username: String,
        email: String,
        password: String
    ): LiveData<Result<User>> = liveData {
        emit(Result.Loading)
        try {
            val user = UserEntity(username = username, email = email, password = password)
            userDao.signUp(user)

            val data = userDao.signIn(email, password)

            createProfile(Profile(userId = data.id))
            prefs.login(data.email)

            emit(Result.Success(data.toDomain()))
        } catch (e: IOException) {
            emit(Result.Error("Failed to create user"))
        } catch (e: Exception) {
            emit(Result.Error(e.localizedMessage?.toString() ?: "Unknown error"))
        }
    }

    override fun signIn(email: String, password: String): LiveData<Result<User>> = liveData {
        emit(Result.Loading)
        try {
            val data = userDao.signIn(email, password)
            prefs.login(data.email)
            emit(Result.Success(data.toDomain()))
        } catch (e: IOException) {
            emit(Result.Error("Failed to sign in"))
        } catch (e: NullPointerException) {
            emit(Result.Error("User not found"))
        } catch (e: Exception) {
            emit(Result.Error(e.localizedMessage?.toString() ?: "Unknown error"))
        }
    }

    override fun getUser(email: String): LiveData<Result<User>> = liveData {
        emit(Result.Loading)
        try {
            val data = userDao.getUser(email)
            emit(Result.Success(data.toDomain()))
        } catch (e: IOException) {
            emit(Result.Error("Failed to retrieve data"))
        } catch (e: NullPointerException) {
            emit(Result.Error("User not found"))
        } catch (e: Exception) {
            emit(Result.Error(e.localizedMessage?.toString() ?: "Unknown error"))
        }
    }

    override suspend fun updateUser(user: User): Result<Int> {
        return try {
            val data = userDao.updateUser(user.toEntity())
            Result.Success(data)
        } catch (e: IOException) {
            Result.Error("Failed to create profile")
        } catch (e: NullPointerException) {
            Result.Error("Profile not found")
        } catch (e: Exception) {
            Result.Error(e.localizedMessage?.toString() ?: "Unknown error")
        }
    }

    override fun getProfile(userId: Int): LiveData<Result<UserAndProfile>> = liveData {
        emit(Result.Loading)
        try {
            val data = userDao.getProfile(userId)
            emit(Result.Success(data))
        } catch (e: IOException) {
            emit(Result.Error("Failed to get profile"))
        } catch (e: NullPointerException) {
            emit(Result.Error("Profile not found"))
        } catch (e: Exception) {
            emit(Result.Error(e.localizedMessage?.toString() ?: "Unknown error"))
        }
    }

    override suspend fun createProfile(profile: Profile): Result<Long> {
        return try {
            val data = userDao.createProfile(profile.toEntity())
            Result.Success(data)
        } catch (e: IOException) {
            Result.Error("Failed to create user")
        } catch (e: Exception) {
            Result.Error(e.localizedMessage?.toString() ?: "Unknown error")
        }
    }

    override suspend fun updateProfile(profile: Profile): Result<Int> {
        return try {
            val data = userDao.updateProfile(profile.toEntity())
            Result.Success(data)
        } catch (e: IOException) {
            Result.Error("Failed to create profile")
        } catch (e: NullPointerException) {
            Result.Error("Profile not found")
        } catch (e: Exception) {
            Result.Error(e.localizedMessage?.toString() ?: "Unknown error")
        }
    }

    override fun getAuthenticatedUser() = prefs.getAuthenticatedEmail().asLiveData()

    override fun logout() = prefs.logout()
}