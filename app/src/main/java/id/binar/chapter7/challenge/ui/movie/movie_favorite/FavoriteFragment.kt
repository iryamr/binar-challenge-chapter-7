package id.binar.chapter7.challenge.ui.movie.movie_favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import id.binar.chapter7.challenge.databinding.FragmentFavoriteBinding
import id.binar.chapter7.challenge.domain.models.Result
import id.binar.chapter7.challenge.domain.models.movie.FavoriteMovie
import id.binar.chapter7.challenge.ui.adapter.MovieVerticalAdapter
import id.binar.chapter7.challenge.utils.Helper.showToast

@AndroidEntryPoint
class FavoriteFragment : Fragment() {

    private var _binding: FragmentFavoriteBinding? = null
    private val binding get() = _binding!!

    private val viewModel: FavoriteViewModel by viewModels()
    private val args: FavoriteFragmentArgs by navArgs()

    private val userId: Int by lazy { args.userId }

    private val movieAdapter: MovieVerticalAdapter<FavoriteMovie> by lazy { MovieVerticalAdapter(::onMovieClicked) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFavoriteBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        doObserveFavoriteMovies()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun doObserveFavoriteMovies() {
        viewModel.getFavoriteMovies(userId).observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> {
                }
                is Result.Success -> {
                    initFavoriteMovies(result.data)
                }
                is Result.Error -> {
                    showToast(requireContext(), result.error)
                }
            }
        }
    }

    private fun initFavoriteMovies(movies: List<FavoriteMovie>) {
        if (movies.isEmpty()) {
            showEmptyState("Favorite movies is empty")
        } else {
            movieAdapter.submitList(movies)
            binding.rvFavoriteMovies.adapter = movieAdapter
            binding.rvFavoriteMovies.layoutManager = initLayoutManager(VERTICAL)
        }
    }

    private fun initLayoutManager(oritentation: Int): LinearLayoutManager {
        return LinearLayoutManager(requireContext(), oritentation, false)
    }

    private fun showEmptyState(message: String) {
        binding.error.tvError.text = message
        binding.error.root.isVisible = true
        binding.rvFavoriteMovies.isVisible = false
    }

    private fun onMovieClicked(movie: FavoriteMovie) {
        val directions = FavoriteFragmentDirections.actionFavoriteFragmentToMovieDetailFragment(
            movie.movieId,
            userId
        )
        findNavController().navigate(directions)
    }

    companion object {
        private const val VERTICAL = LinearLayoutManager.VERTICAL
    }
}