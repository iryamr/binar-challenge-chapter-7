package id.binar.chapter7.challenge.domain.repositories

import androidx.lifecycle.LiveData
import id.binar.chapter7.challenge.domain.models.Result
import id.binar.chapter7.challenge.domain.models.movie.*

interface MovieRepository {

    fun getNowPlayingMovies(): LiveData<Result<List<NowPlayingMovie>>>

    fun getPopularMovies(): LiveData<Result<List<PopularMovie>>>

    fun getFavoriteMovies(userId: Int): LiveData<Result<List<FavoriteMovie>>>

    fun getFavoriteMovie(userId: Int, movieId: Int): LiveData<FavoriteMovie?>

    fun getMovieDetail(movieId: Int): LiveData<Result<MovieDetail>>

    fun getMovieCasts(movieId: Int): LiveData<Result<List<CastDetail>>>

    suspend fun addToFavorite(movie: MovieDetail, userId: Int)

    suspend fun removeFromFavorite(userId: Int, movieId: Int?)
}