package id.binar.chapter7.challenge.ui.movie.movie_detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import id.binar.chapter7.challenge.domain.models.movie.MovieDetail
import id.binar.chapter7.challenge.domain.repositories.MovieRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val repository: MovieRepository
) : ViewModel() {

    fun getMovieDetail(movieId: Int) = repository.getMovieDetail(movieId)

    fun getMovieCasts(movieId: Int) = repository.getMovieCasts(movieId)

    fun getFavoriteMovie(userId: Int, movieId: Int) = repository.getFavoriteMovie(userId, movieId)

    fun addToFavorite(movie: MovieDetail, userId: Int) {
        viewModelScope.launch {
            repository.addToFavorite(movie, userId)
        }
    }

    fun removeFromFavorite(userId: Int, movieId: Int?) {
        viewModelScope.launch {
            repository.removeFromFavorite(userId, movieId)
        }
    }
}