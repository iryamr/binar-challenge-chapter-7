package id.binar.chapter7.challenge.domain.models.movie

import android.os.Parcelable
import id.binar.chapter7.challenge.utils.CommonType
import kotlinx.parcelize.Parcelize

@Parcelize
data class NowPlayingMovie(
    val id: Int,
    val title: String,
    val posterPath: String,
    val overview: String,
    val voteAverage: Double,
) : Parcelable, CommonType