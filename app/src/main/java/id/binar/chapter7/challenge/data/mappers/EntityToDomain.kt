package id.binar.chapter7.challenge.data.mappers

import id.binar.chapter7.challenge.data.sources.local.entities.MovieEntity
import id.binar.chapter7.challenge.data.sources.local.entities.ProfileEntity
import id.binar.chapter7.challenge.data.sources.local.entities.UserEntity
import id.binar.chapter7.challenge.domain.models.auth.Profile
import id.binar.chapter7.challenge.domain.models.auth.User
import id.binar.chapter7.challenge.domain.models.movie.FavoriteMovie

fun MovieEntity.toDomain(): FavoriteMovie =
    FavoriteMovie(
        id = id,
        userId = userId,
        movieId = movieId,
        title = title,
        posterPath = posterPath,
        overview = overview,
        voteAverage = voteAverage,
    )

fun UserEntity.toDomain(): User =
    User(
        id = id,
        username = username,
        email = email,
        password = password,
    )

fun ProfileEntity.toDomain(): Profile =
    Profile(
        id = id,
        userId = userId,
        name = name,
        address = address,
        birthday = birthday,
        photo = photo,
    )