package id.binar.chapter7.challenge.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import id.binar.chapter7.challenge.databinding.ItemMoviesVerticalBinding
import id.binar.chapter7.challenge.domain.models.movie.FavoriteMovie
import id.binar.chapter7.challenge.domain.models.movie.PopularMovie
import id.binar.chapter7.challenge.ui.base.BaseVerticalAdapter
import id.binar.chapter7.challenge.utils.CommonType
import id.binar.chapter7.challenge.utils.Extensions.loadImage
import id.binar.chapter7.challenge.utils.Extensions.toRating

@Suppress("UNCHECKED_CAST")
class MovieVerticalAdapter<T>(
    private val onClick: (T) -> Unit
) : BaseVerticalAdapter<CommonType>() {

    private val popularType = 0
    private val favoriteType = 1

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseVerticalViewHolder<CommonType> {
        val binding =
            ItemMoviesVerticalBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return when (viewType) {
            popularType -> PopularMovieViewHolder(binding) as BaseVerticalViewHolder<CommonType>
            else -> FavoriteMovieViewHolder(binding) as BaseVerticalViewHolder<CommonType>
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (currentList[position]) {
            is PopularMovie -> popularType
            else -> favoriteType
        }
    }

    inner class PopularMovieViewHolder(private val binding: ItemMoviesVerticalBinding) :
        BaseVerticalViewHolder<PopularMovie>(binding.root) {

        override fun onBind(data: PopularMovie) {
            with(binding) {
                tvTitle.text = data.title
                tvOverview.text = data.overview
                ratingBar.rating = data.voteAverage.toRating()
                ivPoster.loadImage(data.posterPath)
                cvPoster.setOnClickListener { onClick(data as T) }
                root.setOnClickListener { onClick(data as T) }
            }
        }
    }

    inner class FavoriteMovieViewHolder(private val binding: ItemMoviesVerticalBinding) :
        BaseVerticalViewHolder<FavoriteMovie>(binding.root) {

        override fun onBind(data: FavoriteMovie) {
            with(binding) {
                tvTitle.text = data.title
                tvOverview.text = data.overview
                ratingBar.rating = data.voteAverage.toRating()
                ivPoster.loadImage(data.posterPath)
                cvPoster.setOnClickListener { onClick(data as T) }
                root.setOnClickListener { onClick(data as T) }
            }
        }
    }
}