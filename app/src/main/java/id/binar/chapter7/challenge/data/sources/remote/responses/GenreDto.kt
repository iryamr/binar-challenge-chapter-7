package id.binar.chapter7.challenge.data.sources.remote.responses

import com.google.gson.annotations.SerializedName

data class GenreDto(

    @SerializedName("id")
    val id: Int,

    @SerializedName("name")
    val name: String,
)
