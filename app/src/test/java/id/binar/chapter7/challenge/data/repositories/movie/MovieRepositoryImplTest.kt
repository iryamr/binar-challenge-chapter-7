package id.binar.chapter7.challenge.data.repositories.movie

import id.binar.chapter7.challenge.data.mappers.toDomain
import id.binar.chapter7.challenge.data.sources.local.room.dao.MovieDao
import id.binar.chapter7.challenge.data.sources.remote.service.ApiService
import id.binar.chapter7.challenge.data.sources.remote.service.FakeApiService
import id.binar.chapter7.challenge.utils.DataDummy
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MovieRepositoryImplTest {

    @Mock
    private lateinit var movieDao: MovieDao
    private lateinit var apiService: ApiService
    private lateinit var repository: MovieRepositoryImpl

    @Before
    fun setUp() {
        apiService = FakeApiService()
        repository = MovieRepositoryImpl(apiService, movieDao)
    }

    @Test
    fun `when getNowPlayingMovies should not null`() = runTest {
        val expectedMovies = DataDummy.generateNowPlayingMovies()
        val actualMovies = apiService.fetchNowPlayingMovies()
        assertNotNull(actualMovies)
        assertEquals(expectedMovies.size, actualMovies.results.size)
    }

    @Test
    fun `when getPopularMovies should not null`() = runTest {
        val expectedMovies = DataDummy.generateNowPlayingMovies()
        val actualMovies = apiService.fetchPopularMovies()
        assertNotNull(actualMovies)
        assertEquals(expectedMovies.size, actualMovies.results.size)
    }

    @Test
    fun `when getMovieDetails should not null`() = runTest {
        val expectedMovies = DataDummy.generateMovieDetail()
        val actualMovies = apiService.fetchMovieDetails(1).toDomain()
        assertNotNull(actualMovies)
        assertEquals(expectedMovies, actualMovies)
    }
}