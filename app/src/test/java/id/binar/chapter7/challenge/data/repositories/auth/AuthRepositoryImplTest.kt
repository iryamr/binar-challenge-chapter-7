package id.binar.chapter7.challenge.data.repositories.auth

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import id.binar.chapter7.challenge.data.mappers.toDomain
import id.binar.chapter7.challenge.data.mappers.toEntity
import id.binar.chapter7.challenge.data.sources.local.room.dao.FakeUserDao
import id.binar.chapter7.challenge.data.sources.local.room.dao.UserDao
import id.binar.chapter7.challenge.utils.DataDummy
import id.binar.chapter7.challenge.utils.UserPreferences
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class AuthRepositoryImplTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var prefs: UserPreferences

    private lateinit var userDao: UserDao
    private lateinit var repository: AuthRepositoryImpl

    @Before
    fun setUp() {
        userDao = FakeUserDao()
        repository = AuthRepositoryImpl(userDao, prefs)
    }

    @Test
    fun `when signUp should exist in signIn`() = runTest {
        val sampleData = DataDummy.generateUsers()[0]
        userDao.signUp(sampleData.toEntity())

        val actualData = userDao.signIn(sampleData.email, sampleData.password)
        assertNotNull(actualData)
    }

    @Test
    fun `when signIn should not null`() = runTest {
        val expectedData = DataDummy.generateUsers()[0]
        val actualData = userDao.signIn(expectedData.email, expectedData.password)
        assertNotNull(actualData)
        assertEquals(expectedData, actualData.toDomain())
    }
}