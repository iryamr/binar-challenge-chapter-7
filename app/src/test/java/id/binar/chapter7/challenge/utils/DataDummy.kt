package id.binar.chapter7.challenge.utils

import id.binar.chapter7.challenge.domain.models.auth.User
import id.binar.chapter7.challenge.domain.models.movie.MovieDetail
import id.binar.chapter7.challenge.domain.models.movie.NowPlayingMovie
import id.binar.chapter7.challenge.domain.models.movie.PopularMovie

object DataDummy {

    fun generateUsers(): List<User> {
        val users = ArrayList<User>()
        for (i in 0..3) {
            val user = User(
                id = 1,
                username = "test",
                email = "test@example.com",
                password = "12345678"
            )
            users.add(user)
        }
        return users
    }

    fun generateNowPlayingMovies(): List<NowPlayingMovie> {
        val movies = ArrayList<NowPlayingMovie>()
        for (i in 0..3) {
            val movie = NowPlayingMovie(
                id = i,
                title = "title",
                posterPath = "posterPath",
                overview = "overview",
                voteAverage = 0.0,
            )
            movies.add(movie)
        }
        return movies
    }

    fun generatePopularMovies(): List<PopularMovie> {
        val movies = ArrayList<PopularMovie>()
        for (i in 0..3) {
            val movie = PopularMovie(
                id = i,
                title = "title",
                posterPath = "posterPath",
                overview = "overview",
                voteAverage = 0.0,
            )
            movies.add(movie)
        }
        return movies
    }

    fun generateMovieDetail(): MovieDetail {
        return MovieDetail(
            id = 1,
            userId = 0,
            title = "1",
            genres = listOf(),
            posterPath = "1",
            backdropPath = "1",
            releaseDate = "1",
            overview = "1",
            voteAverage = 0.0,
        )
    }
}