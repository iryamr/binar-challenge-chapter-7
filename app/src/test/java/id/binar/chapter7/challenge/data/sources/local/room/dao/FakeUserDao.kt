package id.binar.chapter7.challenge.data.sources.local.room.dao

import id.binar.chapter7.challenge.data.mappers.toEntity
import id.binar.chapter7.challenge.data.sources.local.entities.ProfileEntity
import id.binar.chapter7.challenge.data.sources.local.entities.UserAndProfile
import id.binar.chapter7.challenge.data.sources.local.entities.UserEntity
import id.binar.chapter7.challenge.utils.DataDummy

class FakeUserDao : UserDao {

    private val userData = mutableListOf<UserEntity>()

    override suspend fun signUp(user: UserEntity) {
        userData.add(user)
    }

    override suspend fun signIn(email: String, password: String): UserEntity {
        userData.clear()
        return if (userData.isEmpty()) DataDummy.generateUsers()[0].toEntity() else userData[0]
    }

    override suspend fun checkEmail(email: String): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun checkCredentials(email: String, password: String): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun getUser(email: String): UserEntity {
        TODO("Not yet implemented")
    }

    override suspend fun updateUser(profile: UserEntity): Int {
        TODO("Not yet implemented")
    }

    override suspend fun getProfile(userId: Int): UserAndProfile {
        TODO("Not yet implemented")
    }

    override suspend fun createProfile(profile: ProfileEntity): Long {
        TODO("Not yet implemented")
    }

    override suspend fun updateProfile(profile: ProfileEntity): Int {
        TODO("Not yet implemented")
    }
}