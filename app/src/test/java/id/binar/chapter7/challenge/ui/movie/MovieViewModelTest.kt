package id.binar.chapter7.challenge.ui.movie

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import id.binar.chapter7.challenge.data.repositories.movie.MovieRepositoryImpl
import id.binar.chapter7.challenge.domain.models.Result
import id.binar.chapter7.challenge.domain.models.movie.NowPlayingMovie
import id.binar.chapter7.challenge.domain.models.movie.PopularMovie
import id.binar.chapter7.challenge.ui.movie.movie_list.MovieViewModel
import id.binar.chapter7.challenge.utils.DataDummy
import id.binar.chapter7.challenge.utils.getOrAwaitValue
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MovieViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var repository: MovieRepositoryImpl

    private lateinit var viewModel: MovieViewModel

    private val dummyNowPlayingMovies = DataDummy.generateNowPlayingMovies()
    private val dummyPopularMovies = DataDummy.generatePopularMovies()

    @Before
    fun setUp() {
        viewModel = MovieViewModel(repository)
    }

    @Test
    fun `when getNowPlayingMovies should not null and return success`() {
        val expectedValue = MutableLiveData<Result<List<NowPlayingMovie>>>()
        expectedValue.value = Result.Success(dummyNowPlayingMovies)

        `when`(viewModel.getNowPlayingMovies()).thenReturn(expectedValue)

        val actualValue = viewModel.getNowPlayingMovies().getOrAwaitValue()
        verify(repository).getNowPlayingMovies()
        assertNotNull(actualValue)
        assertTrue(actualValue is Result.Success)
        assertEquals(dummyNowPlayingMovies, (actualValue as Result.Success).data)
    }

    @Test
    fun `when getPopularMovies should not null and return success`() {
        val expectedValue = MutableLiveData<Result<List<PopularMovie>>>()
        expectedValue.value = Result.Success(dummyPopularMovies)

        `when`(viewModel.getPopularMovies()).thenReturn(expectedValue)

        val actualValue = viewModel.getPopularMovies().getOrAwaitValue()
        verify(repository).getPopularMovies()
        assertNotNull(actualValue)
        assertTrue(actualValue is Result.Success)
        assertEquals(dummyPopularMovies, (actualValue as Result.Success).data)
    }

    @Test
    fun `when getNowPlayingMovies return Network Error should return error`() {
        val expectedValue = MutableLiveData<Result<List<NowPlayingMovie>>>()
        expectedValue.value = Result.Error("Error")

        `when`(viewModel.getNowPlayingMovies()).thenReturn(expectedValue)

        val actualValue = viewModel.getNowPlayingMovies().getOrAwaitValue()
        verify(repository).getNowPlayingMovies()
        assertNotNull(actualValue)
        assertTrue(actualValue is Result.Error)
    }

    @Test
    fun `when getPopularMovies return Network Error should return error`() {
        val expectedValue = MutableLiveData<Result<List<PopularMovie>>>()
        expectedValue.value = Result.Error("Error")

        `when`(viewModel.getPopularMovies()).thenReturn(expectedValue)

        val actualValue = viewModel.getPopularMovies().getOrAwaitValue()
        verify(repository).getPopularMovies()
        assertNotNull(actualValue)
        assertTrue(actualValue is Result.Error)
    }
}