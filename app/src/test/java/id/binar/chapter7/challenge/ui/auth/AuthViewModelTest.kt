package id.binar.chapter7.challenge.ui.auth

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import id.binar.chapter7.challenge.data.repositories.auth.AuthRepositoryImpl
import id.binar.chapter7.challenge.domain.models.Result
import id.binar.chapter7.challenge.domain.models.auth.User
import id.binar.chapter7.challenge.ui.MainViewModel
import id.binar.chapter7.challenge.utils.DataDummy
import id.binar.chapter7.challenge.utils.getOrAwaitValue
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AuthViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var repository: AuthRepositoryImpl

    private lateinit var viewModel: MainViewModel

    private val dummyUser = DataDummy.generateUsers()[0]

    @Before
    fun setUp() {
        viewModel = MainViewModel(repository)
    }

    @Test
    fun `when signUp should not null and return success`() {
        val expectedValue = MutableLiveData<Result<User>>()
        expectedValue.value = Result.Success(dummyUser)

        `when`(
            viewModel.signUp(
                dummyUser.username,
                dummyUser.email,
                dummyUser.password
            )
        ).thenReturn(expectedValue)

        val actualValue = viewModel.signUp(dummyUser.username, dummyUser.email, dummyUser.password)
            .getOrAwaitValue()
        verify(repository).signUp(dummyUser.username, dummyUser.email, dummyUser.password)
        assertNotNull(actualValue)
        assertTrue(actualValue is Result.Success)
        assertEquals(dummyUser, (actualValue as Result.Success).data)
    }

    @Test
    fun `when signIn should not null and return success`() {
        val expectedValue = MutableLiveData<Result<User>>()
        expectedValue.value = Result.Success(dummyUser)

        `when`(viewModel.signIn(dummyUser.email, dummyUser.password)).thenReturn(expectedValue)

        val actualValue = viewModel.signIn(dummyUser.email, dummyUser.password).getOrAwaitValue()
        verify(repository).signIn(dummyUser.email, dummyUser.password)
        assertNotNull(actualValue)
        assertTrue(actualValue is Result.Success)
        assertEquals(dummyUser, (actualValue as Result.Success).data)
    }

    @Test
    fun `when signUp should null and return error`() {
        val expectedValue = MutableLiveData<Result<User>>()
        expectedValue.value = Result.Error("Error")

        `when`(
            viewModel.signUp(
                dummyUser.username,
                dummyUser.email,
                dummyUser.password
            )
        ).thenReturn(expectedValue)

        val actualValue = viewModel.signUp(dummyUser.username, dummyUser.email, dummyUser.password)
            .getOrAwaitValue()
        verify(repository).signUp(dummyUser.username, dummyUser.email, dummyUser.password)
        assertNotNull(actualValue)
        assertTrue(actualValue is Result.Error)
    }

    @Test
    fun `when signIn should null and return error`() {
        val expectedValue = MutableLiveData<Result<User>>()
        expectedValue.value = Result.Error("Error")

        `when`(viewModel.signIn(dummyUser.email, dummyUser.password)).thenReturn(expectedValue)

        val actualValue = viewModel.signIn(dummyUser.email, dummyUser.password).getOrAwaitValue()
        verify(repository).signIn(dummyUser.email, dummyUser.password)
        assertNotNull(actualValue)
        assertTrue(actualValue is Result.Error)
    }
}