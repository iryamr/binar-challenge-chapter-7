package id.binar.chapter7.challenge.data.sources.remote.service

import id.binar.chapter7.challenge.data.sources.remote.responses.*
import id.binar.chapter7.challenge.utils.DataDummy

class FakeApiService : ApiService {

    private val dummyNowPlayingMovies = DataDummy.generateNowPlayingMovies()
    private val dummyPopularMovies = DataDummy.generatePopularMovies()
    private val dummyMovieDetail = DataDummy.generateMovieDetail()

    override suspend fun fetchNowPlayingMovies(): BaseDto<NowPlayingMovieDto> {
        return BaseDto(dummyNowPlayingMovies.map {
            NowPlayingMovieDto(
                id = it.id,
                title = it.title,
                posterPath = it.posterPath,
                overview = it.overview,
                voteAverage = it.voteAverage
            )
        })
    }

    override suspend fun fetchPopularMovies(): BaseDto<PopularMovieDto> {
        return BaseDto(dummyPopularMovies.map {
            PopularMovieDto(
                id = it.id,
                title = it.title,
                posterPath = it.posterPath,
                overview = it.overview,
                voteAverage = it.voteAverage
            )
        })
    }

    override suspend fun fetchMovieDetails(movieId: Int): MovieDetailDto {
        return MovieDetailDto(
            id = dummyMovieDetail.id,
            title = dummyMovieDetail.title,
            posterPath = dummyMovieDetail.posterPath,
            backdropPath = dummyMovieDetail.backdropPath,
            overview = dummyMovieDetail.overview,
            genres = listOf(),
            releaseDate = dummyMovieDetail.releaseDate,
        )
    }

    override suspend fun fetchMovieCast(movieId: Int): CastDto {
        TODO("Not yet implemented")
    }
}